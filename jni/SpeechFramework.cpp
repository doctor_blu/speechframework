
#include <jni.h>
#include <android/log.h>
#include <fftw3/include/fftw3.h>

#define  LOG_TAG    "SpeechFramework_Cpp"
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

#include <math.h>
#include <iostream>

extern "C"{
JNIEXPORT jdoubleArray JNICALL Java_fr_eurecom_STFT_fftw(JNIEnv* env, jclass clazz, jdoubleArray tmp_fft, jint f_sampling, jintArray cut_freqs, jdoubleArray gain_factors);
}


jdoubleArray Java_fr_eurecom_STFT_fftw(JNIEnv* env, jclass clazz, jdoubleArray tmp_fft, jint f_sampling, jintArray cut_freqs, jdoubleArray gain_factors) {
        //variables
        const jsize fft_length = env->GetArrayLength(tmp_fft);
        double *time_real = env->GetDoubleArrayElements(tmp_fft, 0);
        env->ReleaseDoubleArrayElements(tmp_fft,time_real,0);
        fftw_complex *freq_cpx;
        fftw_plan fft;
        fftw_plan ifft;
        int i = 0;
        int j = 0;

        const jsize n_gains = env->GetArrayLength(gain_factors);
        const jsize n_freqs = env->GetArrayLength(cut_freqs);
        int *i_cuts = env->GetIntArrayElements(cut_freqs, 0);
        double *gains = env->GetDoubleArrayElements(gain_factors, 0);
        double mag, phase;

//      Output vector (freq): N/2+1 elements, DC component is at [0]


//      double f_bin = (f_sampling) / double(fft_length/2+1); //frequency for each i
//      int i_cut = floor(f_cut/f_bin); // how many i required to do the lowpass of the f_cut

        //allocation of memory
        freq_cpx = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*(fft_length/2+1));

        //plan object to do the fft and ifft
        fft = fftw_plan_dft_r2c_1d(fft_length, time_real, freq_cpx, FFTW_ESTIMATE);  //Setup fftw plan for fft
        ifft = fftw_plan_dft_c2r_1d(fft_length, freq_cpx, time_real, FFTW_ESTIMATE);   //Setup fftw plan for ifft

        fftw_execute(fft);


        // FILTERING
        for (j=1; j<n_freqs; j++) { //start from 1: do the comparison between the actual i_cut and the one before
                for (i=i_cuts[j-1]; i<=i_cuts[j]; i++) {
                        if (i < fft_length/2+1) {

                        	mag = sqrt(freq_cpx[i][0]*freq_cpx[i][0]+freq_cpx[i][1]*freq_cpx[i][1]);
                        	phase = atan2(freq_cpx[i][1], freq_cpx[i][0]);
                        	//		omega = (2 * PI * f_bin)/(2 * PI * double(f_cut));
                        	//		butterworth_mag = 1 / sqrt(1 + pow(omega, 2.0));
                        	//		butterworth_phase = - 1.0 * atan(omega);

                        	//		freq_cpx[i][0] = mag * butterworth_mag * cos(phase);
                        	//		freq_cpx[i][1] = mag * butterworth_phase * sin(phase);
                            freq_cpx[i][0] = gains[j-1] * mag * cos(phase); //0.5 + 0.5*cos(double(PI*i/(double(i_cut))));
                            freq_cpx[i][1] = gains[j-1] * mag * sin(phase);
                        }
                }
        }


        fftw_execute(ifft);

        //NORMALIZE DATA OUTPUT: divide by the size of the transform
        for (i = 0; i < fft_length; i++){
                time_real[i] = time_real[i]/(double)fft_length;
        }


        //release resources
        env->SetDoubleArrayRegion(tmp_fft, 0, fft_length, time_real);
        env->ReleaseDoubleArrayElements(gain_factors, gains, 0);
        env->ReleaseIntArrayElements(cut_freqs, i_cuts, 0);

        fftw_destroy_plan(fft);
        fftw_destroy_plan(ifft);
        fftw_free(freq_cpx);
//      fftw_free(time_real);
//      free(gains);
//      free(i_cuts);

        return tmp_fft;

}


