LOCAL_PATH := $(call my-dir)
ROOT_PATH := $(LOCAL_PATH)

include $(call all-subdir-makefiles)
include $(CLEAR_VARS)

LOCAL_PATH := $(ROOT_PATH)
LOCAL_MODULE    := SpeechFramework
LOCAL_SRC_FILES := SpeechFramework.cpp
LOCAL_LDLIBS    := -llog
LOCAL_STATIC_LIBRARIES := fftw3

include $(BUILD_SHARED_LIBRARY)