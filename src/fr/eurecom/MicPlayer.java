package fr.eurecom;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Environment;

/**
 * Records audio from the microphone, applies the STFT and outputs data to the loudspeaker and/or to a file.
 * To be used in a separate Thread.
 * <i>At the moment, the performances are very low. To be rewritten in Native (OpenSL ES)</i>
 *
 */
public class MicPlayer implements Runnable {

		//set true for stopping the player and ending the thread
		private boolean stop = true;
		
		//OutputStream
		private OutputStream os;
		
		//STFT and params
		private STFT stft;
		private int SAMP_FREQ;
		private int CHANNEL_CONFIG_OUT;
		private float BUFFER_FACTOR = 1.0f;
		private int buf_len_at;
		private int buf_len_ar;
		private double[] gain_factors;
		private boolean playOut;
		
		//Recorder
		private AudioRecord ar;
		
		//Player
		private AudioTrack at;
				
		
		/**
		 * Records audio from the microphone, applies the STFT and outputs data to the loudspeaker and/or to a file. (To be used in a separate Thread)
		 * <i>At the moment, the performances are very low. To be rewritten in Native (OpenSL ES)</i>
		 * 
		 * @param frame_lenght_ms Frame Lenght in ms
		 * @param frame_shift_ratio FrameLenght/FrameShift factor (eg: 2 = 50% overlap)
		 * @param samp_freq Sample Frequency (Hz) for the audio captured by the microphone (usually, 8000 or 16000)
		 * @param outputFileName Name of the output file (in the directory <i>/mnt/sdcard/media/audio</i> ). If null or "", the file won't be written.
		 * @param num_subBands How many sub bands are used for applying gain (kind of EQ)
		 * @param out_loudspeaker True if the sound has to be routed to the loudspeaker
		 */
		public MicPlayer(int frame_lenght_ms, int frame_shift_ratio, int samp_freq, String outputFileName, int num_subBands, boolean out_loudspeaker) {
			
			playOut = out_loudspeaker;
			
			//sampling frequency
			this.SAMP_FREQ = samp_freq;
			
			gain_factors = new double[num_subBands];
			for (int i=0; i<gain_factors.length; i++) {
				gain_factors[i] = 1.0;
			}
			
			// registering from the microphone = MONO
			this.CHANNEL_CONFIG_OUT = AudioFormat.CHANNEL_OUT_MONO;
			
			//ask for the minimum buffer sizes (ar_buffer should be about 0.5*at_buffer)
			buf_len_at = (int) Math.floor(BUFFER_FACTOR * AudioTrack.getMinBufferSize(SAMP_FREQ, CHANNEL_CONFIG_OUT, AudioFormat.ENCODING_PCM_16BIT));
			buf_len_ar = (int) Math.floor(BUFFER_FACTOR * AudioRecord.getMinBufferSize(SAMP_FREQ, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT));
			
			//create STFT class with parameters
			stft = new STFT(frame_shift_ratio, frame_lenght_ms, samp_freq, buf_len_ar, num_subBands);
			
			//create the player with parameters
			at = new AudioTrack(AudioManager.STREAM_MUSIC, SAMP_FREQ, CHANNEL_CONFIG_OUT, AudioFormat.ENCODING_PCM_16BIT, buf_len_at, AudioTrack.MODE_STREAM);
			at.setPlaybackRate(SAMP_FREQ);
			
			//create the recorder with parameters
			ar = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMP_FREQ, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, buf_len_ar);
			
			//set output stream
			if (outputFileName != null && outputFileName != "") {
				
				//check folder
				File out_dir = new File(Environment.getExternalStorageDirectory().getPath(), "media/audio");
				if (!out_dir.exists())
					out_dir.mkdirs();
				
				//check output file
				File out_file = new File(Environment.getExternalStorageDirectory().getPath(),"media/audio/"+outputFileName);
	            if (out_file.exists())
	            	out_file.delete();

	            //create stream
	            try {
					os = new FileOutputStream(out_file);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		public void setStop(boolean b) {
			this.stop = b;
		}
		
		
		/**
		 * Set the gain for the desired sub-band of frequency
		 * @param g Gain
		 * @param band_number Sub-band
		 */
		public void setGain(double g, int band_number) {
			if (band_number >= 0 && band_number < gain_factors.length)
				this.gain_factors[band_number] = g;
		}
		
		public int getSampFreq() {
			return this.SAMP_FREQ;
		}

		@Override
		public void run() {
			Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

			if (at!=null)
				at.play();
			
			if (ar!=null)
				ar.startRecording();
			
			stop = false;
			
			byte[] buffer = new byte[buf_len_ar];
			
//			long time = System.currentTimeMillis();
	        
	        try {
				while (!stop && at!=null && ar!=null) {
					
//					Log.d("MIC_PLAYER_ELAPSED", ""+(System.currentTimeMillis()-time));
//					time = System.currentTimeMillis();
					
					ar.read(buffer, 0, buf_len_ar);
					
					//STFT operations
					stft.frameBuffer(buffer);
					stft.fourierAnalysis(gain_factors);
					stft.buildBuffer(buffer);
						
					// Data to loudspeaker
					if (playOut)
						at.write(buffer, 0, buf_len_ar);
					
					// Data to file
					if (os!=null)
						os.write(buffer, 0, buf_len_ar);

					
				}
				
				if (at != null) {
		        	at.stop();
					at.flush();
					at.release();
					at = null;
		        }
				
				if (ar != null) {
					ar.stop();
					ar.release();
					ar = null;
				}
		        
		        if (os != null) {
		        	os.close();
		        	os = null;
		        }
		        
		        if (stft != null) stft = null;
		        
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
}
