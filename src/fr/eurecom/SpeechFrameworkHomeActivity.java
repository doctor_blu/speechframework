package fr.eurecom;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class SpeechFrameworkHomeActivity extends Activity {
	
	// Parameters
	private int FRAME_LENGHT = 32; //STFT param: frame lenght = 32ms
	private int FRAME_SHIFT_RATIO = 2; //STFT param: frame shift = 16ms
	private boolean SOURCE_FILE = true; // TRUE = File, FALSE = Mic
	private boolean OUT_FILE = false;
	private boolean OUT_SPEAKER = true;
	private int NUM_SUBBANDS = 5;
	
	//FRAMEWORK
	private SpeechFramework sf;
	
	//Layout variables
	private TextView freqLabel1, freqLabel2, freqLabel3, freqLabel4, freqLabel5;
	private SeekBar freqSeekBar1, freqSeekBar2, freqSeekBar3, freqSeekBar4, freqSeekBar5;
	private EditText outFilename;
	private RadioGroup srcRadioGroup;
	private ToggleButton outFile, outSpeaker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_speech_framework_home);
		
		sf = new SpeechFramework(FRAME_LENGHT, FRAME_SHIFT_RATIO, NUM_SUBBANDS);
		
		freqLabel1 = (TextView) findViewById(R.id.cutfreq_TextView1);
		freqLabel2 = (TextView) findViewById(R.id.cutfreq_TextView2);
		freqLabel3 = (TextView) findViewById(R.id.cutfreq_TextView3);
		freqLabel4 = (TextView) findViewById(R.id.cutfreq_TextView4);
		freqLabel5 = (TextView) findViewById(R.id.cutfreq_TextView5);
		
		freqSeekBar1 = (SeekBar) findViewById(R.id.cutfreq_SeekBar1);
		freqSeekBar2 = (SeekBar) findViewById(R.id.cutfreq_SeekBar2);
		freqSeekBar3 = (SeekBar) findViewById(R.id.cutfreq_SeekBar3);
		freqSeekBar4 = (SeekBar) findViewById(R.id.cutfreq_SeekBar4);
		freqSeekBar5 = (SeekBar) findViewById(R.id.cutfreq_SeekBar5);
		
		outFilename = (EditText) findViewById(R.id.outFilename_editText);
		
		freqLabel1.setText(""+freqSeekBar1.getProgress()/(double)100);
		freqLabel2.setText(""+freqSeekBar2.getProgress()/(double)100);
		freqLabel3.setText(""+freqSeekBar3.getProgress()/(double)100);
		freqLabel4.setText(""+freqSeekBar4.getProgress()/(double)100);
		freqLabel5.setText(""+freqSeekBar5.getProgress()/(double)100);
		
		outFile = (ToggleButton) findViewById(R.id.outFile_ToggleButton);
		outSpeaker = (ToggleButton) findViewById(R.id.outSpeaker_toggleButton);
		
		srcRadioGroup = (RadioGroup) findViewById(R.id.source_radioGroup);
		
		//Set SeekBar listener
		freqSeekBar1.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				freqLabel1.setText(""+progress/(double)100);
				try {
					sf.setGain(progress/(double)100, 0);
				} catch (PlayerException e) {
					e.printStackTrace();
				}
			}
		});
		freqSeekBar2.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				freqLabel2.setText(""+progress/(double)100);
				try {
					sf.setGain(progress/(double)100, 1);
				} catch (PlayerException e) {
					e.printStackTrace();
				}
			}
		});
		freqSeekBar3.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				freqLabel3.setText(""+progress/(double)100);
				try {
					sf.setGain(progress/(double)100, 2);
				} catch (PlayerException e) {
					e.printStackTrace();
				}
			}
		});
		freqSeekBar4.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				freqLabel4.setText(""+progress/(double)100);
				try {
					sf.setGain(progress/(double)100, 3);
				} catch (PlayerException e) {
					e.printStackTrace();
				}
			}
		});
		freqSeekBar5.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) { }
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				freqLabel5.setText(""+progress/(double)100);
				try {
					sf.setGain(progress/(double)100, 4);
				} catch (PlayerException e) {
					e.printStackTrace();
				}
			}
		});
		
		//Set RadioGroup Listener
		srcRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (checkedId == R.id.filesrc_radio)
					SOURCE_FILE = true;
				else if (checkedId == R.id.micsrc_radio)
					SOURCE_FILE = false;
			}
		});
		
		
		//Set ToggleButtons Listener
		outFile.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				OUT_FILE = ((ToggleButton) v).isChecked();
			}
		});
		
		outSpeaker.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				OUT_SPEAKER = ((ToggleButton) v).isChecked();
			}
		});
		
	}
	
	
	// START BUTTON IS CLICKED
	public void playAudio(View v) {
		
		try {
			sf.setOutFilename(outFilename.getText().toString()+".pcm");
			
			if (SOURCE_FILE) {
				InputStream is = this.getAssets().open("male.wav");
				sf.startFilePlayer(is, OUT_SPEAKER, OUT_FILE);
			} else {
				int FREQ = 8000;
				sf.startMicPlayer(FREQ, OUT_SPEAKER, OUT_SPEAKER);
			}
			
			sf.setGain(freqSeekBar1.getProgress()/(double)100, 0);
			sf.setGain(freqSeekBar2.getProgress()/(double)100, 1);
			sf.setGain(freqSeekBar3.getProgress()/(double)100, 2);
			sf.setGain(freqSeekBar4.getProgress()/(double)100, 3);
			sf.setGain(freqSeekBar5.getProgress()/(double)100, 4);
		
		} catch (PlayerException e) {
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	// STOP BUTTON IS CLICKED (or application is closed)
	public void stopAudio(View v) {
		
		try {
			sf.stopPlayer();
		} catch (PlayerException e) {
			if (v!=null)
				Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.speech_framework_home, menu);
		return true;
	}
	
	@Override
	protected void onPause() {
		this.stopAudio(null);
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		this.stopAudio(null);
		super.onDestroy();
	}

}
