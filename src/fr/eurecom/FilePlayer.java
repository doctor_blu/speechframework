package fr.eurecom;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Environment;
import android.util.Log;

/**
 * Reads a wave file, applies the STFT and outputs data to the loudspeaker and/or to a file.
 * To be used in a separate Thread.
 *
 */
public class FilePlayer implements Runnable {
	
	//set true for stopping the player and close the thread
	private boolean stop = true;
	
	//InputStream
	private InputStream is;
	private DataInputStream dis;
	
	//OutputStream
	private OutputStream os;
	
	//STFT and params
	private STFT stft;
	private int SAMP_FREQ;
	private int ENCODING_CONFIG = AudioFormat.ENCODING_DEFAULT;
	private int CHANNEL_CONFIG_OUT = AudioFormat.CHANNEL_OUT_MONO;
	private float BUFFER_FACTOR = 1.0f;
	private int buf_len;
	private double[] gain_factors;
	private boolean playOut;
	
	//Player
	private AudioTrack at;
	
	/**
	 * Plays a wave file, applies the STFT and outputs it to the loudspeaker and/or to a file. (To be used in a separate Thread)
	 * 
	 * @param frame_lenght_ms Frame Lenght in ms
	 * @param frame_shift_ratio FrameLenght/FrameShift factor (eg: 2 = 50% overlap)
	 * @param inputFile Opened InputStream object of the wave file to read
	 * @param outputFileName Name of the output file (in the directory <i>/mnt/sdcard/media/audio</i> ). If null or "", the file won't be written.
	 * @param num_subBands How many sub bands are used for applying gain (kind of EQ)
	 * @param out_loudspeaker True if the sound has to be routed to the loudspeaker
	 */
	public FilePlayer(int frame_lenght_ms, int frame_shift_ratio, InputStream inputFile, String outputFileName, int num_subBands, boolean out_loudspeaker) {
		
		playOut = out_loudspeaker;
		
		gain_factors = new double[num_subBands];
		for (int i=0; i<num_subBands; i++) {
			gain_factors[i] = 1.0;
		}
		
		//set input stream
		is = inputFile;
		if (is!=null)
			dis = new DataInputStream(inputFile);
		
		//read wave metadata and set the right parameters
		if (dis!=null)
			parseWaveHeader();
		
		//ask for the minimum buffer size
		buf_len = (int) Math.floor(BUFFER_FACTOR * AudioTrack.getMinBufferSize(SAMP_FREQ, CHANNEL_CONFIG_OUT, ENCODING_CONFIG));
		
		//create STFT class with parameters
		stft = new STFT(frame_shift_ratio, frame_lenght_ms, SAMP_FREQ, buf_len, num_subBands);
		
		//create the player with parameters
		at = new AudioTrack(AudioManager.STREAM_MUSIC, SAMP_FREQ, CHANNEL_CONFIG_OUT, ENCODING_CONFIG, buf_len, AudioTrack.MODE_STREAM);
		
		//set output stream
		if (outputFileName != null && outputFileName != "") {
			
			//check folder
			File out_dir = new File(Environment.getExternalStorageDirectory().getPath(), "media/audio");
			if (!out_dir.exists())
				out_dir.mkdirs();
			
			//check output file
			File out_file = new File(Environment.getExternalStorageDirectory().getPath(),"media/audio/"+outputFileName);
            if (out_file.exists())
            	out_file.delete();

            //create stream
            try {
				os = new FileOutputStream(out_file);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void setStop(boolean b) {
		this.stop = b;
	}
	
	/**
	 * Set the gain for the desired sub-band of frequency
	 * @param g Gain
	 * @param band_number Sub-band
	 */
	public void setGain(double g, int band_number) {
		if (band_number >= 0 && band_number < gain_factors.length) {
			this.gain_factors[band_number] = g;
		}
	}
	
	public int getSampFreq() {
		return this.SAMP_FREQ;
	}

	@Override
	public void run() {
		Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

		if (at!=null)
			at.play();
		
		stop = false;
		
		int count = 0;
		byte[] buffer = new byte[buf_len];
		
//		long time = System.currentTimeMillis();
        
        try {
			while (!stop && dis!=null && (count = dis.read(buffer, 0, buf_len)) >= 0 && at!=null) {
				
//				Log.d("FILE_PLAYER_ELAPSED", ""+(System.currentTimeMillis()-time));
//				time = System.currentTimeMillis();
				
				//STFT operations
				stft.frameBuffer(buffer);
				stft.fourierAnalysis(gain_factors);
				stft.buildBuffer(buffer);
									
				// Data to loudspeaker
				if (playOut)
					at.write(buffer, 0, count);
				
				// Data to file
				if (os!=null)
					os.write(buffer, 0, count);
			}
			
			if (at != null) {
	        	at.stop();
				at.flush();
				at.release();
				at = null;
	        }
			
			if (dis != null) {
				dis.close();
				dis = null;
			}
			
	        if (is != null) {
	        	is.close();
	        	is = null;
	        }
	        
	        
	        if (os != null) {
	        	os.close();
	        	os = null;
	        }
	        
	        if (stft != null) stft = null;
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void parseWaveHeader() {
		
		byte[] tmpLong = new byte[4];
		byte[] tmpInt = new byte[2];

		long myDataSize = 0;
		
		try {
			String chunkID ="" + (char)dis.read() + (char)dis.read() + (char)dis.read() + (char)dis.read();
			
			// read the ChunkSize
			dis.read(tmpLong);
//			long myChunkSize = byteArrayToLong(tmpLong);
			
			// read file format
			String fileFormat = "" + (char)dis.read() + (char)dis.read() + (char)dis.read() + (char)dis.read();

			// the subChunk1 ID
			String subChunk1ID = "" + (char)dis.read() + (char)dis.read() + (char)dis.read() + (char)dis.read();
					    
			// read the SubChunk1 Size
			dis.read(tmpLong);
//			long SubChunk1Size = byteArrayToLong(tmpLong);
			
			// read the audio format (1 for PCM)
			dis.read(tmpInt);
			int audioFormat = byteArrayToInt(tmpInt);
			
			// read the # of channels (1 or 2)
			dis.read(tmpInt);
			int numChannels = byteArrayToInt(tmpInt);

			// read the samplerate
			dis.read(tmpLong);
			int sampleRate = (int)byteArrayToLong(tmpLong);
			
			// read the byterate
			dis.read(tmpLong);
//			long byteRate = byteArrayToLong(tmpLong);

			// read the blockalign
			dis.read(tmpInt);
//			int blockAlign = byteArrayToInt(tmpInt);

			// read the bitspersample
			dis.read(tmpInt);
			int bitsPerSample = byteArrayToInt(tmpInt);

			// read the ID of the data Chunk
			String dataChunkID = "" + (char)dis.read() + (char)dis.read() + (char)dis.read() + (char)dis.read();
			
			// read the size of the data
			dis.read(tmpLong); // read the size of the data
//			long dataSize = byteArrayToLong(tmpLong);
			
			Log.d("WAVE PARSE", "FileFormat: "+fileFormat+" // AudioFormat: "+audioFormat+" // NumChannels: "+numChannels+" // SampleRate: "+sampleRate+" // BitsPerSample: "+bitsPerSample);
			
			//set sampling frequency
			this.SAMP_FREQ = sampleRate;
			
			//set encoding format 
			if (audioFormat==1)
				if (bitsPerSample == 16)
					ENCODING_CONFIG = AudioFormat.ENCODING_PCM_16BIT;
				else if (bitsPerSample == 8)
					ENCODING_CONFIG = AudioFormat.ENCODING_PCM_8BIT;
				else
					ENCODING_CONFIG = AudioFormat.ENCODING_INVALID;
			
			//set num channels (mono vs stereo)
			if (numChannels == 1)
				this.CHANNEL_CONFIG_OUT = AudioFormat.CHANNEL_OUT_MONO;
			else if (numChannels == 2)
				this.CHANNEL_CONFIG_OUT = AudioFormat.CHANNEL_OUT_STEREO;
					
		
		} catch (Exception e) {
			Log.d("WAVE PARSE", "Exception");
			e.printStackTrace();
		}
	}
	
	public static long byteArrayToLong(byte[] b) {
		int start = 0;
		int i = 0;
		int len = 4;
		int cnt = 0;
		byte[] tmp = new byte[len];
		for (i = start; i < (start + len); i++)
		{
			tmp[cnt] = b[i];
			cnt++;
		}
		long accum = 0;
		i = 0;
		for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 )
		{
			accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
			i++;
		}

		return accum;
	}

	public static int byteArrayToInt(byte[] b) {
		int start = 0;
		int low = b[start] & 0xff;
		int high = b[start+1] & 0xff;
		return (int)( high << 8 | low );
	}
	
}
