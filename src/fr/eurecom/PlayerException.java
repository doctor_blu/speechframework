package fr.eurecom;

public class PlayerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6288767623524183067L;

	public PlayerException(String s) {
		super(s);
	}
	
}
