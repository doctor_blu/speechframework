package fr.eurecom;

import java.io.InputStream;

/**
 * @author marco
 *
 * Class for using the framework; Initialize one instance of this class, and use it for playing/stopping sound
 * The framework manages by himself the thread and the player state.
 *
 */
public class SpeechFramework {
	
	private int frame_length, frame_shift_ratio;
	private int num_bands;
	
	private String out_filename;
	
	private Thread playThread;
	private FilePlayer filePlayer;
	private MicPlayer micPlayer;
	
	/**
	 * 
	 * Create SpeechFramwork instance
	 * 
	 * @param frame_length: lenght of STFT frame (ms)
	 * @param frame_shift_ratio: ratio between frame_Lenght and frame_shift (usually 2)
	 * @param sub_bands: number of sub bands to which apply gains (kind of EQ)
	 */
	public SpeechFramework(int frame_length, int frame_shift_ratio, int sub_bands) {
		this.frame_length = frame_length;
		this.frame_shift_ratio = frame_shift_ratio;
		this.num_bands = sub_bands;
		this.out_filename = "";
	}
	
	/**
	 * Play a wave file
	 * 
	 * @param filein InputStream opened on the file to read
	 * @param out_loudspeaker True if the audio has to be routed to the loudspeaker
	 * @param out_file True if the audio has to be written into a .pcm file
	 * @throws PlayerException happens if a player is already playing a sound
	 */
	public void startFilePlayer(InputStream filein, boolean out_loudspeaker, boolean out_file) throws PlayerException {
		if (filePlayer==null && micPlayer==null && playThread == null) {
			
			String f="";
			if (out_file)
				f = this.out_filename;
			
			filePlayer = new FilePlayer(frame_length, frame_shift_ratio, filein, f, num_bands, out_loudspeaker);
			playThread = new Thread(filePlayer);
			playThread.start();
		} else throw new PlayerException("Sound is already playing");
	}
	
	/**
	 * Play sound from microphone
	 * 
	 * @param sampl_freq Sampling frequency of the microphone recording
	 * @param out_loudspeaker True if the audio has to be routed to the loudspeaker
	 * @param out_file True if the audio has to be written into a .pcm file
	 * @throws PlayerException happens if a player is already playing a sound
	 */
	public void startMicPlayer(int sampl_freq, boolean out_loudspeaker, boolean out_file) throws PlayerException {
		if (filePlayer==null && micPlayer==null && playThread == null) {
			
			String f="";
			if (out_file)
				f = this.out_filename;
				
			micPlayer = new MicPlayer(frame_length, frame_shift_ratio, sampl_freq, f, num_bands, out_loudspeaker);
			playThread = new Thread(micPlayer);
			playThread.start();
			
		} else throw new PlayerException("Sound is already playing");
	}
	
	/**
	 * Stop the player
	 * 
	 * @throws PlayerException happens if no player is playing
	 */
	public void stopPlayer() throws PlayerException {
		if ((filePlayer!=null || micPlayer!=null) && playThread != null) {
			
			if (filePlayer != null) {
				filePlayer.setStop(true);
				filePlayer = null;
			}
			if (micPlayer != null) {
				micPlayer.setStop(true);
				micPlayer = null;
			}
			
			playThread = null;
		} else throw new PlayerException("No player to stop");
	}
	
	
	/**
	 * Set the gain for a specified sub_band
	 * 
	 * @param g gain factor (no gain is 1.0)
	 * @param band subband number (NB: the first sub band is the number 0)
	 * @throws PlayerException happens if subband index is out of bound with respect to the array (and the declared number of subbands)
	 */
	public void setGain(double g, int band) throws PlayerException {
		if (band >= 0 && band<this.num_bands) {
			if (filePlayer!=null)
				filePlayer.setGain(g, band);
			else if (micPlayer!=null)
				micPlayer.setGain(g, band);
		} else
			throw new PlayerException("The selected subband is out of bounds");
	}
	
	/**
	 * Set the entire gains array
	 * 
	 * @param gains gains array (the size of the array has to be equal to the number of subbands)
	 * @throws PlayerException happens if the array lenght is different from the number of subbands
	 */
	public void setGains(double[] gains) throws PlayerException {
		for (int i=0; i<gains.length; i++)
			this.setGain(gains[i], i);
	}
	
	public void setOutFilename(String s) {
		out_filename = s;
	}
	
}
