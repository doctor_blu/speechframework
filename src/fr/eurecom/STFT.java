package fr.eurecom;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import android.util.Log;

/**
 * Performs, on each buffer, the whole STFT procedure, included some frequency filters.
 *
 */
public class STFT {
	
	private int fs_ratio, fl; //frame shift and frame length in ms
	private int n_fs, n_fl; //frame shift and length in samples
	private int buf_len; //length of the buffer array (bytes)
	private int data_len; //length of the buffer array (converted to short)
	private int padded_data_len; //put 0 padding before and after the buffer short[] data
	private float n_segs; //number of frames that can be taken from one buffer array
	private float[][] stft_matrix;
	private float[] window; //Hamming coefficient
	private boolean search_norm_factor = true;
	private int i,j,k; //index for loops
	private ByteBuffer bb;
	private int f_sampling;
	private float[] tmp_buf;
	private double[] tmp_fft;
	private double[] tmp_ifft;
	private int[] cut_freqs;
	private int FFT_SIZE = 1024;
	
	/**
	 * Creates an STFT object, that can perform on each buffer all the stages of the processing:
	 * <ul>
	 * 	<li>Framing and windowing</li>
	 * 	<li>FFT, filtering and IFFT</li>
	 * 	<li>Overlap-Add</li>
	 * </ul>
	 * Data is stored inside the STFT matrix
	 * 
	 * @param frame_shift_ratio FrameLenght/FrameShift factor (eg: 2 = 50% overlap)
	 * @param frame_length Frame Lenght in ms
	 * @param freq Sampling Frequency
	 * @param buf_len Lenght of the buffers that will be processed
	 * @param num_subBands Number of SubBands for filtering 
	 */
	public STFT(int frame_shift_ratio, int frame_length, int freq, int buf_len, int num_subBands) {
		this.fs_ratio = frame_shift_ratio;
		this.fl = frame_length;
		this.buf_len = buf_len;
		this.data_len = buf_len/2;		
		this.f_sampling = freq;
			
		//compute values from ms to samples
		n_fl = (int) Math.floor(fl*freq/1000);
		n_fs = n_fl / this.fs_ratio;
		
		padded_data_len = 2*n_fl + data_len;
		
		//create coefficients
		window = hann(n_fl);

		//initialize tmp_buffer (used in windowing and overlap-add)
		tmp_buf = new float[padded_data_len];
		
		//initialize ByteBuffer (for conversion Byte-short)
		bb = ByteBuffer.allocateDirect(2);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		
		//compute how many frames can be extracted from the buffer
		n_segs = 1 + (float) (Math.ceil((this.padded_data_len-n_fl)/n_fs));
		
		//data matrix: size of frame (with padding from previous frame) * number of segments
		stft_matrix = new float[n_fl][(int)n_segs];
		
		Log.d("STFT STATS", "BufLen:"+this.buf_len+" // Flen:"+n_fl+" // FSh:"+n_fs+
				" // Nsegs:"+n_segs);
						
		//buffers for FFT data, with zero padding
		tmp_fft= new double[FFT_SIZE];
		tmp_ifft = new double[FFT_SIZE];
		
		for (int i=0; i<FFT_SIZE; i++) {
			tmp_fft[i] = 0;
			tmp_ifft[i] = 0;
		}		
		
		//cut frequencies for subBands
		cut_freqs = new int[num_subBands+1];
		
		for (j=0; j<num_subBands+1; j++) {
			cut_freqs[j] = (int) (FFT_SIZE/2+1) * j / num_subBands;
		}
		
	}
	
	
	/**
	 * Framing and windowing operations:
	 * <ul>
	 * 	<li>Converts the whole buffer from byte to float</li>
	 * 	<li>Divides the whole buffer into frames</li>
	 * 	<li>Applies Hann window coefficients</li>
	 * 	<li>Store each frame into the STFT matrix</li>
	 * </ul>
	 * 
	 * @param buf Data buffer to be framed
	 */
	public void frameBuffer(byte[] buf) {
		
		//initialize tmp_buffer and add 0 padding
		for (k=0; k<padded_data_len; k++)
			tmp_buf[k] = 0;
		
		//fill the short[] buffer converting from byte[] buffer
		
		for (i=0; i<buf_len; i+=2) {
			bb.position(0);
			bb.put(buf[i]);
			bb.put(buf[i+1]);
			tmp_buf[n_fl+i/2] = (float) bb.getShort(0);
		}
		
//		Log.d("IN VALUES", tmp_buf[n_fl] +" "+ tmp_buf[n_fl+57]+" "+ tmp_buf[n_fl+111]+" "+ tmp_buf[n_fl+200]);
		
		
		//frame the short[] buffer into the matrix using windowing
		for (j=0; j<n_segs; j++) {
			for (int i=0; i<n_fl; i++) {
				stft_matrix[i][j] = tmp_buf[j*n_fs+i]*window[i];
			}
		}

	}
	
	/**
	 * Overlap-Add operations:
	 * <ul>
	 * 	<li>Sums all the frames into the STFT matrix in one buffer</li>
	 * 	<li>Converts the whole buffer from float to byte</li>
	 * </ul>
	 * 
	 * @param output Buffer on which data will be written
	 */
	
	//sums all frames from STFT matrix into one buffer
	public void buildBuffer(byte[] output) {
		
		//initialize tmp_buffer and add 0 padding
		for (k=0; k<padded_data_len; k++)
			tmp_buf[k] = 0;
				
		//Overlap-Add
		for (j=0; j<n_segs; j++) {
			for (i=0; i<n_fl; i++) {
				tmp_buf[j*n_fs+i] += stft_matrix[i][j];
			}
		}
		
		//convert from short[] to byte[]
		for (i=0; i<buf_len; i+=2) {
			bb.position(0);
			bb.putShort( (short) (tmp_buf[n_fl+i/2]));
			output[i] = bb.get(0);
			output[i+1] = bb.get(1);
		}
		
		
				
	}
	
	/**
	 * For each frame in the STFT matrix:
	 * 
	 * <ul>
	 * 	<li>Applies FFT</li>
	 * 	<li>Filter Data</li>
	 * 	<li>Applies IFFT</li>
	 * </ul>
	 * @param f_cut Cut frequency of the filter
	 */
	public void fourierAnalysis(double[] gain_factors) {
				
		for (j=0; j<n_segs;j++) {
			
			for (i=0; i<n_fl; i++)
				tmp_fft[i] = (double) stft_matrix[i][j];
			
			//native method that implements fft and ifft, with fftw3 library
			tmp_ifft = fftw(tmp_fft, f_sampling, cut_freqs, gain_factors);
			
			for (int i=0; i<n_fl; i++)
				stft_matrix[i][j] = (float) tmp_ifft[i];
		}
		
		
	}
	
	
	//utility method for Hamming coefficients
	private float[] hamming(int len){
		float[] win = new float[len];
		for (i=0; i<len; i++){
			win[i] = (float) (0.54-0.46*Math.cos((2*Math.PI*i)/(len-1)));
		}
		return win;
	}
	
	//utility method for Hann coefficients
	private float[] hann(int len){
		float[] win = new float[len];
		for (i=0; i<len; i++){
			win[i] = (float) (0.5*(1-Math.cos((2*Math.PI*i)/(len-1))));
		}
		
		return win;
	}
	
	
	//NATIVE METHODS
	
	native double[] fftw(double[] tmp_fft, int f_sampling, int[] cut_freqs, double[] gain_factors);
	static {
        System.loadLibrary("SpeechFramework");
    }


}
